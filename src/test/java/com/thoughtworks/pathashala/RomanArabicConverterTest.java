package com.thoughtworks.pathashala;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RomanArabicConverterTest {
    @Nested
    class RomanToArabicTest {
        @Nested
        class TestPredefinedValues {
            @Test
            void expectValue1WhenRomanNumeralIs_I() {
                RomanNumeral romanOne = new RomanNumeral("I");
                assertEquals(1, romanOne.numericalValue());
            }

            @Test
            void expectValue1000WhenRomanNumeralIs_M() {
                RomanNumeral romanThousand = new RomanNumeral("M");
                assertEquals(1000, romanThousand.numericalValue());
            }
        }

        @Nested
        class TestMixedRomanValues {
            @Test
            void expect3RomanNumeralIs_III() {
                RomanNumeral romanSixty = new RomanNumeral("III");
                assertEquals(3, romanSixty.numericalValue());
            }

            @Test
            void expectValue60WhenRomanNumeralIs_LX() {
                RomanNumeral romanSixty = new RomanNumeral("LX");
                assertEquals(60, romanSixty.numericalValue());
            }

            @Test
            void expectValue2006WhenRomanNumeralIs_MMVI() {
                RomanNumeral romanTwoThousandSix = new RomanNumeral("MMVI");
                assertEquals(2006, romanTwoThousandSix.numericalValue());
            }

            @Test
            void expectValue1944WhenRomanNumeralIs_MCMXLIV() {
                RomanNumeral romanTwoThousandSix = new RomanNumeral("MCMXLIV");
                assertEquals(1944, romanTwoThousandSix.numericalValue());
            }
        }
    }

    @Nested
    class ArabicToRomanTest {
        @Nested
        class TestPredefinedValues {
            @Test
            void expectRoman_I_WhenArabicNumeralIs_1() {
                ArabicNumeral arabicOne = new ArabicNumeral(1);
                assertEquals("I", arabicOne.romanValue());
            }

            @Test
            void expectRoman_L_WhenArabicNumeralIs_50() {
                ArabicNumeral arabicFifty = new ArabicNumeral(50);
                assertEquals("L", arabicFifty.romanValue());
            }

            @Test
            void expectRoman_IX_WhenArabicNumeralIs_9() {
                ArabicNumeral arabicNine = new ArabicNumeral(9);
                assertEquals("IX", arabicNine.romanValue());
            }
        }

        @Nested
        class TestMixedRomanValues {
            @Test
            void expectRoman_LX_WhenArabicNumeralIs_60() {
                ArabicNumeral arabicSixty = new ArabicNumeral(60);
                assertEquals("LX", arabicSixty.romanValue());
            }

            @Test
            void expectRoman_L_WhenArabicNumeralIs_50() {
                ArabicNumeral arabicTwoThousandSix = new ArabicNumeral(2006);
                assertEquals("MMVI", arabicTwoThousandSix.romanValue());
            }

            @Test
            void expectRoman_MCMXLIV_WhenArabicIs_1944() {
                ArabicNumeral arabicOneThousandNineHundredAndFortyFour = new ArabicNumeral(1944);
                assertEquals("MCMXLIV", arabicOneThousandNineHundredAndFortyFour.romanValue());
            }
        }
    }

    @Nested
    class AssignmentStringTest {
        @Test
        void expectRoman_I_WhenStringIs_Fischas_Is_I() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("Fischas is I");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals("I", RomanNumeral.romanValueOf("Fischas"));

        }

        @Test
        void expectRoman_V_WhenStringIs_Jahakes_Is_V() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("Jahakes is V");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals("V", RomanNumeral.romanValueOf("Jahakes"));

        }

        @Test
        void expectRoman_X_WhenStringIs_Yeraan_Is_X() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("Yeraan is X");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals("X", RomanNumeral.romanValueOf("Yeraan"));

        }

        @Test
        void expectRoman_L_WhenStringIs_Sheiraki_Is_L() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("Sheiraki is L");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals("L", RomanNumeral.romanValueOf("Sheiraki"));

        }
    }

    @Nested
    class ProductStringTest {
        @Test
        void expectColdIron_36_WhenStringIs_Fischas_Is_I() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("Fichas is I");
                add("Fichas Fichas Cold Iron is 72 coins");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals(36, Product.productValue("Cold Iron"));

        }

        @Test
        void expectMithril_12000_WhenStringIs_Fischas_Is_I_And_Jahekas_Is_V() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("Fichas is I");
                add("Jahekas is V");
                add("Fichas Jahekas Mithril is 48000 coins");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals(12000, Product.productValue("Mithril"));

        }
    }

    @Nested
    class AnswerTest {
        @Test
        void expect_test_output_WhenStringsAre_TestInputs() {
            List<String> inputStringList = new ArrayList<String>() {{
                add("fichas is I");
                add("jahakes is V");
                add("yeraan is X");
                add("shieraki is L");
                add("fichas fichas Cold Iron is 72 coins");
                add("fichas jahakes Mithril is 48000 coins");
                add("yeraan yeraan Hihi’irokane is 20000 coins");
                add("how much is yeraan shieraki fichas jahakes?");
                add("how many coins is fichas jahakes Cold Iron?");
                add("how many coins is fichas fichas Mithril?");
                add("how many coins is yeraan Hihi’irokane?");
                add("how much money would I need to retire in Essos?");
            }};
            ControllerClass controller = new ControllerClass(inputStringList);
            assertEquals(new ArrayList<Integer>() {{
                add(44);
                add(144);
                add(24000);
                add(10000);
                add(-1);
            }}, controller.answers());
            //assertEquals(1000,Product.productValue("Hihi’irokane"));
        }
    }
}
