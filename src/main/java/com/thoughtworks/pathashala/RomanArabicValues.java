package com.thoughtworks.pathashala;
//Holds predefined Roman Numerals
enum RomanArabicValues {
    ONE("I", 1), FOUR("IV", 4), FIVE("V", 5), NINE("IX", 9), TEN("X", 10), FOURTY("XL", 40), FIFTY("L", 50), NINETY("XC", 90), HUNDRED("C", 100), FOUR_HUNDRED("CD", 400), FIVE_HUNDRED("D", 500), NINE_HUNDRED("CM", 900), THOUSAND("M", 1000);
    private String symbol;
    private int numericalValue;

    RomanArabicValues(String symbol, int numericalValue) {
        this.symbol = symbol;
        this.numericalValue = numericalValue;
    }

    String getSymbol() {
        return symbol;
    }

    int getNumericalValue() {
        return numericalValue;
    }
}
