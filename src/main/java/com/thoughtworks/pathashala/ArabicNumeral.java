package com.thoughtworks.pathashala;
//Stores and performs operations on Arabic numbers.
public class ArabicNumeral {
    private int arabicNumber;

    public ArabicNumeral(int arabicNumber) {
        this.arabicNumber = arabicNumber;
    }

    public String romanValue() {
        StringBuilder romanValue = new StringBuilder();
        for (int i = RomanArabicValues.values().length - 1; i >= 0; i--) {
            RomanArabicValues currentRomanArabicValue = RomanArabicValues.values()[i];
            while (arabicNumber >= currentRomanArabicValue.getNumericalValue()) {
                arabicNumber -= currentRomanArabicValue.getNumericalValue();
                romanValue.append(currentRomanArabicValue.getSymbol());
            }
        }
        return romanValue.toString();
    }
}
