package com.thoughtworks.pathashala;

import java.util.ArrayList;
import java.util.List;
//Stores and performs operations on  RomanNumerals
public class RomanNumeral {
    private String roman;
    private String name;
    private static List<RomanNumeral> romanNumeralList = new ArrayList<>();

    public RomanNumeral() {
    }

    public RomanNumeral(String symbol) {
        this.roman = symbol;
    }

    private RomanNumeral(String symbol, String name) {
        this(symbol);
        this.name = name;
    }

    static boolean isValidRoman(String roman) {
        for (RomanArabicValues currentRoman : RomanArabicValues.values()) {
            if (currentRoman.getSymbol().equals(roman)) {
                return true;
            }
        }
        return false;
    }

    static boolean isValidDothraki(String dothraki) {
        for (RomanNumeral currentRomanNumeral : romanNumeralList) {
            if (currentRomanNumeral.name.equals(dothraki)) {
                return true;
            }
        }
        return false;
    }

    public static String romanValueOf(String dothraki) {
        for (RomanNumeral currentRomanNumeral : romanNumeralList) {
            if (currentRomanNumeral.name.equals(dothraki)) {
                return currentRomanNumeral.roman;
            }
        }
        return "";
    }

    static void createRomanNumeral(String name, String romanNumeralValue) {
        RomanNumeral romanNumeral = new RomanNumeral(romanNumeralValue, name);
        romanNumeralList.add(romanNumeral);

    }


    public int numericalValue() {
        int numericalValue = 0, i;
        for (i = 0; i < roman.length() - 1; i++) {
            int numericalValueOfCurrentSymbol = mumericalValueOfCurrentSymbol(Character.toString(roman.charAt(i)));
            int numericalValueOfNextSymbol = mumericalValueOfCurrentSymbol(Character.toString(roman.charAt(i + 1)));

            if (numericalValueOfCurrentSymbol < numericalValueOfNextSymbol) {
                numericalValue += (numericalValueOfNextSymbol - numericalValueOfCurrentSymbol);
                i++;
            } else {
                numericalValue += numericalValueOfCurrentSymbol;
            }
        }
        if (i < roman.length()) {
            numericalValue += mumericalValueOfCurrentSymbol(Character.toString(roman.charAt(i)));
        }
        return numericalValue;
    }

    private int mumericalValueOfCurrentSymbol(String symbol) {
        for (RomanArabicValues currentRoman : RomanArabicValues.values()) {
            if (currentRoman.getSymbol().equals(symbol)) {
                return currentRoman.getNumericalValue();
            }
        }
        return 0;
    }
}
