package com.thoughtworks.pathashala;


import java.util.ArrayList;
import java.util.List;
public class ControllerClass {
    private List<Integer> answerList = new ArrayList<>();

    public ControllerClass(List<String> inputStringList) {
        for (String inputString : inputStringList) {
            if (isQuestion(inputString)) {
                if (irrelevantQuestion(inputString)) {
                    answerList.add(-1);
                } else {
                    answerList.add(answer(inputString));
                }
            }
            if (isAssignment(inputString)) {
                String name = parseName(inputString);
                String romanNumeralValue = parseRomanNumeral(inputString);
                RomanNumeral.createRomanNumeral(name, romanNumeralValue);
            } else if (isProduct(inputString)) {
                int quantityInRoman = quantity(inputString, 0);
                String productName = productName(inputString);
                int productValue = productValue(inputString) / quantityInRoman;
                Product.createProduct(productName, productValue);
            }
        }
    }

    public List<Integer> answers() {
        return answerList;
    }

    private boolean irrelevantQuestion(String inputString) {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == ' ') {
                if (RomanNumeral.isValidDothraki(temp.toString())) {
                    return false;
                }
                temp = new StringBuilder();
            } else {
                temp.append(inputString.charAt(i));
            }
        }
        return true;
    }

    private int answer(String inputString) {
        int quantity = quantity(inputString, inputString.indexOf("is") + 3);
        int productValue = Product.productValue(productNameFromQuestion(inputString));
        if (productValue != 0) {
            return quantity * productValue;
        }
        return quantity;
    }

    private String productNameFromQuestion(String inputString) {
        int isIndex = inputString.indexOf("is");
        StringBuilder temp = new StringBuilder();
        String productName = "";
        for (int i = isIndex + 3; i < inputString.length(); i++) {
            if (inputString.charAt(i) == ' ' || inputString.charAt(i) == '?') {
                if (!RomanNumeral.isValidDothraki(temp.toString()) && inputString.charAt(i) == '?') {
                    productName = temp.toString();
                }
                if (!RomanNumeral.isValidDothraki(temp.toString()) && inputString.charAt(i) == ' ') {
                    productName = temp.toString() + inputString.substring(i, inputString.length() - 1);
                    break;
                }
                temp = new StringBuilder();
            } else {
                temp.append(inputString.charAt(i));
            }
        }
        return productName;
    }

    private boolean isQuestion(String inputString) {
        return inputString.contains("how") && inputString.contains("?");
    }

    private int productValue(String inputString) {
        int isIndex = inputString.indexOf("is");
        int spaceAfterIsIndex = inputString.indexOf(" ", isIndex + 3);
        StringBuilder productValue = new StringBuilder();
        for (int i = isIndex + 3; i < spaceAfterIsIndex; i++) {
            productValue.append(inputString.charAt(i));
        }
        return Integer.parseInt(productValue.toString());
    }

    private String productName(String inputString) {
        StringBuilder productName = new StringBuilder();
        String tempString = "";
        int i;
        int isIndex = inputString.indexOf("is");
        for (i = 0; i < isIndex; i++) {
            if (inputString.charAt(i) == ' ') {
                if (!RomanNumeral.isValidDothraki(tempString)) {
                    for (i = i + 1; i < isIndex - 1; i++) {
                        productName.append(inputString.charAt(i));
                    }
                }
            } else {
                tempString += Character.toString(inputString.charAt(i));
            }
        }
        return productName.toString();
    }

    private int quantity(String inputString, int startIndex) {
        StringBuilder roman = new StringBuilder();
        StringBuilder tempDothrakiString = new StringBuilder();
        for (int i = startIndex; i < inputString.length(); i++) {
            if (inputString.charAt(i) == ' ' || inputString.charAt(i) == '?') {
                if (RomanNumeral.isValidDothraki(tempDothrakiString.toString())) {
                    roman.append(RomanNumeral.romanValueOf(tempDothrakiString.toString()));
                }
                tempDothrakiString = new StringBuilder();
            } else {
                tempDothrakiString.append(inputString.charAt(i));
            }
        }
        return new RomanNumeral(roman.toString()).numericalValue();
    }

    private boolean isProduct(String inputString) {
        String firstWord = firstWord(inputString);
        return RomanNumeral.isValidDothraki(firstWord) && isProductValuePresent(inputString);
    }

    private boolean isProductValuePresent(String inputString) {
        int i;
        for (i = 0; i < inputString.length(); i++) {
            if (Character.isDigit(inputString.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private String parseRomanNumeral(String inputString) {
        return thirdWord(inputString);
    }

    private String parseName(String inputString) {
        int i = 0;
        StringBuilder name = new StringBuilder();
        while (inputString.charAt(i) != ' ') {
            name.append(inputString.charAt(i));
            i++;
        }
        return name.toString();
    }

    private boolean isAssignment(String inputString) {
        return countSpaces(inputString) == 2 && countWords(inputString) == 3 && isRomanNumeralPresent(inputString);
    }

    private boolean isRomanNumeralPresent(String inputString) {
        String thirdWord = thirdWord(inputString);
        return RomanNumeral.isValidRoman(thirdWord);
    }

    private String firstWord(String inputString) {
        String firstWord = "";
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) != ' ') {
                firstWord += inputString.charAt(i);
            } else {
                return firstWord;
            }
        }
        return firstWord;
    }

    private String thirdWord(String inputString) {
        int indexOfFirstSpace = inputString.indexOf(" ");
        int indexOfSecondSpace = inputString.indexOf(" ", indexOfFirstSpace + 1);
        StringBuilder thirdWord = new StringBuilder();
        for (int i = indexOfSecondSpace + 1; i < inputString.length(); i++) {
            thirdWord.append(inputString.charAt(i));
        }
        return thirdWord.toString();
    }

    private int countWords(String inputString) {
        return countSpaces(inputString) + 1;
    }

    private int countSpaces(String inputString) {
        int count = 0;
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == ' ') {
                count++;
            }
        }
        return count;
    }

}
