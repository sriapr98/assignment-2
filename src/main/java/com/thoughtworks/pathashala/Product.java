package com.thoughtworks.pathashala;
//Stores details of an item
import java.util.ArrayList;
import java.util.List;

class Product {
    private final String productName;
    private final int productValue;
    private static List<Product> productList = new ArrayList<>();

    private Product(String productName, int productValue) {
        this.productName = productName;
        this.productValue = productValue;
    }

    static void createProduct(String productName, int productValue) {
        productList.add(new Product(productName, productValue));
    }

    static int productValue(String productName) {
        for (Product currentProduct : productList) {
            if (currentProduct.productName.equals(productName)) {
                return currentProduct.productValue;
            }
        }
        return 0;
    }
}
